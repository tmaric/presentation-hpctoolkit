# Strip the / at the end, LaTex freaks out.
HANDOUT="-V handout"
# Compress the navigational circles into a single line.
#THEMEOPTION="-V themeoption:compress"

pandoc \
    -s -t beamer \
    --biblatex \
    --bibliography=literature.bib \
    -V theme:Berlin \
    $THEMEOPTION \
    -V fontsize:9pt \
    -V date:"March 2017" \
    $(find $1 -name options -exec cat {} \;) \
    -V colortheme:dove \
    $HANDOUT \
    --filter filter_columns.py \
    --filter filter_visible.py \
    --filter filter_center.py \
    --filter filter_vspace.py \
    --filter filter_hline.py \
    00-title/title.md \
    01-hpctoolkit/00-intro.md \
    01-hpctoolkit/01-profiling-overview.md \
    01-hpctoolkit/02-hpctoolkit-handson.md \
    -o hpctoolkit.tex

pdflatex hpctoolkit.tex 
