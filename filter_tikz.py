import pandocfilters as pf

def latex(s):
    return pf.RawBlock('latex', s)

def mk_tikz(k, v, f, m):
    if k == "Para":
        value = pf.stringify(v)
        if value.startswith('[') and value.endswith(']'):
            content = value[1:-1]
            if content.startswith("tikz="):
                return latex(r'\begingroup%
                        \tikzset{every picture/.style={scale=%s}%' %(content[7:]))
                        \input{%s}' % content[7:])


            \begingroup
            \tikzset{every picture/.style={scale=0.3}}%
            \input{sometikzpic}%
            \endgroup

if __name__ == "__main__":
    pf.toJSONFilter(mk_tikz)
