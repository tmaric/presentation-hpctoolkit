import pandocfilters as pf

def latex(s):
    return pf.RawBlock('latex', s)

def mk_vspace(k, v, f, m):
    if k == "Para":
        value = pf.stringify(v)
        if value.startswith('[') and value.endswith(']'):
            content = value[1:-1]
            if content.startswith("hline"):
                return latex(r'\begin{tikzpicture}\draw[sourcefluxBlue] (0,0) -- (\textwidth,0); \end{tikzpicture}')

if __name__ == "__main__":
    pf.toJSONFilter(mk_vspace)
