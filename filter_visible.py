import pandocfilters as pf

def latex(s):
    return pf.RawBlock('latex', s)

def mk_visible(k, v, f, m):
    if k == "Para":
        value = pf.stringify(v)
        if value.startswith('[') and value.endswith(']'):
            content = value[1:-1]
            if content.startswith("visible="):
                return latex(r'\visible<%s>{' % content[8:])
            elif content == "/visible":
                return latex(r'}')

if __name__ == "__main__":
    pf.toJSONFilter(mk_visible)
