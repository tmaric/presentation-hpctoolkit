import pandocfilters as pf

def latex(s):
    return pf.RawBlock('latex', s)

def mk_vspace(k, v, f, m):
    if k == "Para":
        value = pf.stringify(v)
        if value.startswith('[') and value.endswith(']'):
            content = value[1:-1]
            if content.startswith("vspace="):
                return latex(r'\vspace{%s}' % content[7:])

if __name__ == "__main__":
    pf.toJSONFilter(mk_vspace)
