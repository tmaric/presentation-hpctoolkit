## HPCToolkit Hands-on Tutorial 

### Main measurement parameters (events) 

**Event**: stuff that a piece of hardware did that can be measured.

Based on previous slides, we will measure 

1. Time spent in a part (context) of the code.  
2. Time spent in fetching data from different caches. 
3. Number of mispredicted branches.  
4. Total number of executed branches. 

- **Measureable events are platform-dependent**: 
- List events with `hpcrun -L`

### What is profiled in this hands-on tutorial? 

- Me: 
    - My programs.  
- You: 
    - Your own program if you wish. 
    - Make sure your program is compiled with 
        - all optimization turned on, 
        - full debugging support. 

### Building OpenFOAM for profiling with HPCToolkit I 

- `Prof` configuration option in `$WM_PROJECT_DIR/etc/bashrc` sets


~~~
        c++DBUG    = -pg
        c++OPT     = -O2
~~~

- We need   


~~~
        c++DBUG    = -ggdb3 -DFULLDEBUG
        c++OPT     = -O3
~~~


- Create the files

~~~{.bash}
?> cd $WM_PROJECT_DIR/wmake/rules
?> cp linux64Gcc/c++Opt linux64Gcc/c++Hpc  
?> cp linux64Gcc/cOpt linux64Gcc/cHpc
~~~

### Building OpenFOAM for profiling with HPCToolkit II

Change the files

- `$WM_PROJEC_DIR/wmake/rules/c++Hpc`

~~~
    c++DBUG    = -ggdb3 -DFULLDEBUG
    c++OPT     = -O3
~~~

- `$WM_PROJECT_DIR/wmake/rules/cHpc`

~~~
    cDBUG       = -ggdb3 -DFULLDEBUG
    cOPT        = -O3
~~~

- `$WM_PROJECT_DIR/etc/bashrc` 

~~~{.bash}
    #- Optimised, debug, profiling:
    #    WM_COMPILE_OPTION = Opt | Debug | Prof
    export WM_COMPILE_OPTION=Hpc
~~~

Execute OpenFOAM build script. 

### Profiling CPU time spent on a context 

- Measuring the time spent in parallel: 

~~~{.bash}
    ?> SUB mpirun -np N hpcrun -t PROGRAM OPTIONS
~~~

- Parameters:
    - `SUB`: queue submit command of your cluster 
    - `N`: number of processes
    - `PROGRAM`: name of your program 
    - `OPTIONS`: options of your program
    - `-t` : call path **trace** (measure threads or processes) 
- `hpcrun` is inserted before the name of your application.
- It seamlessly measures the overhead of Open MPI functions. 
- In serial, just use 

~~~{.bash}
    ?> hpcrun -t PROGRAM OPTIONS
~~~

### Profiling structure and call paths 

- Relating program structure to measurements

~~~{.bash}
    ?> hpcstruct `which PROGRAM` 
~~~

- Creating the call path structure  

~~~{.bash}
    ?> hpcprof -S PROGRAM.struct \
    -I /absolute/path/to/program/source-directory \
    hpctoolkit-PROGRAM-measurements
~~~

- Running multiple times $\rightarrow$ measurements are appended with process ID.


### Result analysis with `hpcviewer`

Viewing results

~~~{.bash}
    ?> hpcviewer hpctoolkit-PROGRAM-database
~~~

\begin{figure}
    \includegraphics[width=0.7\textwidth]{figures/hpcviewer-overview.png}
\end{figure}

### Isolating bottlenecks 

Finding an Open MPI overhead

\begin{figure}
    \includegraphics[width=0.7\textwidth]{figures/hpcviewer-mpi.png}
\end{figure}

- Finding serial/parallel bottlenecks is easy. 
    - Use the context browser. 
    - Click on a source code link or line number.
- The context browser links to source code. 
- HPCToolkit connects things like loops to spent resources. 


### Listing additional events

- To get a list of available events, execute

~~~{.bash}
    ?> hpcrun -L 
    PAPI_L1_DCM	Yes	Level 1 data cache misses
    PAPI_L1_ICM	Yes	Level 1 instruction cache misses
    PAPI_L2_DCM	No	Level 2 data cache misses
    PAPI_L2_ICM	Yes	Level 2 instruction cache misses
    PAPI_L1_TCM	No	Level 1 cache misses
    PAPI_L2_TCM	Yes	Level 2 cache misses
    PAPI_L3_TCM	Yes	Level 3 cache misses
    PAPI_CA_SNP	Yes	Requests for a snoop
~~~

### Measuring additional events

- Measuring wall clock time, branch instructions, mispredictions ...  

~~~{.bash}
    ?> hpcrun -t -e WALLCLOCK@5000 -e PAPI_BR_INS@100000 \
    -e PAPI_BR_MSP@100000 -e PAPI_L1_DCM@100000 \
    -e PAPI_L2_TCM@100000 \
    PROGRAM OPTIONS
~~~

- Serial syntax

~~~{.bash}
    ?> hpcrun -e EVENT1@FREQUENCY1 EVENT2@FREQUENCY2 ... PROGRAM  
~~~

- Parallel syntax: just add the event part to the parallel call.

### Creating derived metrics 

- Additional events given by `hpcrun -L` may not be enough.  
- Example:
    - number of branch mispredictions, vs. 
    - number of branch mispredictions / number of branch executions.
- Adding derived metrics

\begin{figure}
    \includegraphics[width=0.3\textwidth]{figures/derived-metrics.png}
    \includegraphics[width=0.5\textwidth]{figures/mspr-rel.png}
\end{figure}

### Alternative views 

- Flat view: shows the guilty parts of your program.

\begin{figure}
    \includegraphics[width=0.7\textwidth]{figures/flat-view.png}
\end{figure}

- Another reason for writing clean code.

### Parallel load imbalances I 

- Use `hpctraceviewer` to check the **Trace View** 

~~~{.bash}
    ?> hpctraceviewer hpctoolkit-PROGRAM-database
~~~

\begin{figure}
    \includegraphics[width=0.8\textwidth]{figures/trace-view.png}
\end{figure}

- Rows: processes.  
- Horizontal axis: wall clock time. 
- Colors: contexts (parts of your program). 

### Parallel load imbalances II  

- Still checking **Trace View**.
- White cross: a selected point in (time, process) space. 
- Code description: 
    - Stack call trace of the program in the white cross point.  
    - Each trace line colors the process differently.
    - Immediate overview of **parallel granularity**. 
    - **Parallel granularity**: ratio of calculation to communication. 

\begin{figure}
    \includegraphics[width=0.8\textwidth]{figures/trace-detail.png}
\end{figure}

### Parallel load imbalances III

- Not quite done with **Trace View**.
- A "sub-optimal" granularity of the example program: 

\begin{figure}
    \includegraphics[width=0.8\textwidth]{figures/trace-granularity.png}
\end{figure}

- Wide blue (purple? I'm almost colorblind) columns: **MPI_Waitall**
- Everything else: some calculations.
- **Keep the thickness of MPI_Whatever columns as small as possible.** 

### Parallel load imbalances III

**Depth View**

\begin{figure}
    \includegraphics[width=0.4\textwidth]{figures/depth-02.png}
    \includegraphics[width=0.4\textwidth]{figures/depth-03.png}
\end{figure}

- Left figure: computation interrupted with communication a lot.  
    - Breaks, pauses in calculation. 
- Right figure: calculation is **heterogeneous**.
    - Oscillations in calculation intensity. 
- Goal: **keep the peak calculation count as constant as possible.** 
- Question: where do the oscillations come from?
    - `if-then-else`: varying logical conditions in the calculation space. 

### Improving serial efficiency 

- Introduce cheap tests that avoid costly calculations.  
- Change your data structures. 
- Working on numerical methods:
    - Profiling tells you what your **unit operation** is. 
    - Analyze the complexity of your method in terms of the unit operation.  
    - If the efficiency is crazily bad: consider alternative algorithms. 
- Learn how to program code that anticipates changes. 
    - Without introducing an overhead for added flexibility! 

### Improving parallel efficiency: domain decomposition 

Domain decomposition

- Interleave communication and computation.  
    - Use non-blocking communication. 
    - This fails for the example in the tutorial: `MPI_Waitall` problem. 
- Avoid overuse of collectives.
- Use a stable and recent MPI implementation. 
- Read about MPI compilation flags that might help.

### Improving parallel efficiency: shared memory 

Shared memory (OpenMP)

- Check if you can make use of nested loop parallelism.
- Use thread-private variables for interim solutions.
- Try to write into global lists after thread-local calculations. 
- Avoid `critical` in threaded loops.
- Experiment with scheduling models (static, guided, dynamic, etc).


### What next? 

[Click me.](http://hpctoolkit.org/documentation.html) 
