## Mechanical engineer's perspective on HPC 

### What does the compiler do?  

- Translates a programming language into CPU "instruction language".
- The CPU has small capacity: it fetches stuff from memory. 
- The C++ compiler optimization reduces fetching + execution: 
    - Nested loop variables can be *registered* physically near the CPU.
    - No return copies are made 
        - (Named) Return Value Optimization (N)RVO
        - `x = f(y,z)` : construct result of `f` in-place of `x`
    - Constant variables are specially treated: use `const`. 
    - Short functions can be inlined: avoids fetching function instructions.
    - Avoid calling virtual functions in long loops. 
        - Their code is always fetched.
- "Optimizing software in C++" by Agner Fog. 

### What does a CPU roughly do?  

- The CPU understands instructions and data.  
    1. It fetches the instructions and data from memory. 
    2. It executes the instructions on data. 
    3. It writes the results to memory.


### Why do I have to read about how memory works? 

- **Fetching stuff takes time.**
- Memory has a hierarchy: close to and far from CPU.
- Memory close to the CPU: **cache memory**.
    - Has multiple (up to 3) levels (modern x86_64 architecture). 
- Instructions executed many times are pre-fetched and re-used.  
- Consecutive data access: pre-fetched into cache in chunks. 
    - `for(auto & v : mesh.points())` :
    - Fetches "64" bits worth of `point` and stores this in the cache.
- **Try to avoid unordered element access in long lists.** 
    - Unstructured meshes.... :( 
- "What every programmer should know about memory" by Ulrich Drepper. 
 
### What's the problem with doing many logical instructions? 

- The CPU does not simply evaluate a logical instruction: 
    - **Branch predictor**: a hardware circuit that guesses `true` or `false`.  
- **Avoid uniformly random** `if-then-else` **tests in long loops**.
    - Probability of `false` and `true` is similar $\rightarrow$ predictor fails $\rightarrow$ **bottleneck**. 
    - If the condition is seldom `false` or `true` it's maybe OK: profile.  

### Shared memory parallelism and cache memory 

- NUMA: Non Uniform Memory Access, not the Romanian pop song :)
    - CPUs today have many cores. 
    - Cores are physically split $\rightarrow$ their caches are physically split. 
    - Accessing another cores' memory is expensive $\rightarrow$ try to avoid this.
- Cache coherency (cc)NUMA
    - A variable in the cache has a twin in the main memory.  
    - Cache variable modified $\rightarrow$ its twin needs an update. 
    - Cache variable modified $\rightarrow$ flagged as not-coherent. 
    - Cache variable modified $\rightarrow$ entire cache line is flagged. 
    - Programmers must be aware of this: can cause bottlenecks.
- "OpenMP does not scale", a great talk by Ruud van der Pas.

### How to anticipate code optimization in C++

- Use C++ templates to abstract data structures and algorithms 
    - Instantiated templates can be plugged into an object oriented hierarchy. 
    - You can specialize templates for data structures, algorithms, etc.
    - Policy based design: avoids virtual functions, offers easy change of behavior.
    - Policy member functions are often short and inlined: no object oriented overhead.
- There is no such thing as a data structure-agnostic algorithm. 
- Use well tested algorithms from established libraries first. 
    - Consider programming new ones yourself later when optimizing. 
- **Do not optimize too early: make the program run correctly.** 
