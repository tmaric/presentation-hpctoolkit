# HPCToolkit Tutorial

## Outline 

### What is the HPCToolkit? 

* It is a set of **simple tools for detailed profiling.**  
* **It does not require code modification.**
* It supports many different measurements. 
* **It works with OpenMP or Open MPI.** 
* It scales very well on a large number of processes.

### Learning goals 

How to profile your code with HPCtoolkit? 

- Find bottlenecks (e.g. for loops) that take up **time**.
- Find out if there is an **time** imbalance in a parallel code. 

What to do once the **time bottlenecks** are found?

- What is the bottleneck doing? 
    - Learn in a nutshell about possible time sinks.  
    - What should we **measure additional to time**?
- How to reduce the computational costs. 

### How do I improve computational efficiency? 

In a nutshell, 

1. Compile your code with **full debug** and **maximal optimization**.
2. Use `hpcrun` for measuring: **1-5\%** overhead. 
3. Use `hpcstruct` to relate the code structure to the measurements. 
    - This relates object to source code.
    - Identifies costly *contexts*: e.g. loops.
4. Use `hpcprof` to overlay the measurements with the source code. 
    - This constructs the call path structure. 
5. Inspect with `hpcviewer` or `hpctraceviewer` to find bottlenecks. 
6. Change your source code to improve efficiency.  
7. Goto 1. 

